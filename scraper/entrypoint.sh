#!/bin/sh

python scraper.py -i data_samples/test.csv

# Below is too long to be executed only for tests.
#python scraper.py -i data_samples/270.csv
#python scraper.py -i data_samples/208.csv