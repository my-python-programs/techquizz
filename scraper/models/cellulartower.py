from sqlalchemy import Column
from sqlalchemy.types import Integer, String, DECIMAL
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class CellularTower(Base):
    __tablename__ = "api_cellulartower"

    radio = Column(String(30))
    mcc = Column(Integer)
    net = Column(Integer)
    area = Column(Integer)
    cell = Column(Integer, primary_key=True)
    unit = Column(Integer)
    lon = Column(DECIMAL)
    lat = Column(DECIMAL)
    range = Column(Integer)
    samples = Column(Integer)
    changeable = Column(Integer)
    created = Column(Integer)
    updated = Column(Integer)
    averageSignal = Column(Integer)