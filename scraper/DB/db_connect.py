from .config import MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST_IP, MYSQL_PORT, MYSQL_DATABASE
from sqlalchemy import create_engine, Table, Column, Integer, String, DECIMAL, MetaData
from sqlalchemy.schema import MetaData
from sqlalchemy.orm import sessionmaker


def create_table():
    if not engine.dialect.has_table(engine, 'api_cellulartower'):
        metadata = MetaData(engine)
        cellulartowers = Table('api_cellulartower', metadata,
            Column('radio', String(30)),
            Column('mcc', Integer),
            Column('net', Integer),
            Column('area', Integer),
            Column('cell', Integer, primary_key=True),
            Column('unit', Integer),
            Column('lon', DECIMAL),
            Column('lat', DECIMAL),
            Column('range', Integer),
            Column('samples', Integer),
            Column('changeable', Integer),
            Column('created', Integer),
            Column('updated', Integer),
            Column('averageSignal', Integer),
        )
        metadata.create_all(engine)


engine = create_engine('mysql://'\
    + MYSQL_USER 
    + ':'
    + MYSQL_PASSWORD
    + '@'
    + MYSQL_HOST_IP
    + ':'
    + str(MYSQL_PORT)
    + '/'
    + MYSQL_DATABASE, echo=False)

conn = engine.connect()
db_table = 'api_cellulartower'
metadata = MetaData(bind=engine)
create_table()
table = Table(db_table, metadata, autoload=True)
Session = sessionmaker(bind=engine)
session = Session()