import os

MYSQL_USER = os.environ.get('DB_USER')
MYSQL_PASSWORD = os.environ.get('DB_PASSWORD')
MYSQL_HOST_IP = os.environ.get('DB_HOST')
MYSQL_PORT = os.environ.get('DB_PORT')
MYSQL_DATABASE = os.environ.get('DB_NAME')
