from django.db import models

class CellularTower(models.Model):
    cell = models.IntegerField(default=None, primary_key=True)
    radio = models.CharField(default=None, max_length=30)
    mcc = models.IntegerField(default=None)
    net = models.IntegerField(default=None)
    area = models.IntegerField(default=None)
    unit = models.IntegerField(default=None)
    lon = models.DecimalField(default=None, decimal_places=10, max_digits=12)
    lat = models.DecimalField(default=None, decimal_places=10, max_digits=12)
    range = models.IntegerField(default=None)
    samples = models.IntegerField(default=None)
    changeable = models.IntegerField(default=None)
    created = models.IntegerField(default=None)
    updated = models.IntegerField(default=None)
    averageSignal = models.IntegerField(default=None)
