from api.models.cellulartower import CellularTower


def filter_cellulartower_from_db(queryset, params):
    # I think that using query parameters this way is easy to maintain. You can easily add a new filter for the MySQL request
    # by updating the list 'get_filter_parameters'
    query_filter_parameters = ['mcc', 'net', 'area', 'cell']
    filters = {}
    for parameter in query_filter_parameters:
        if params.get(parameter):
            filters[parameter] = params[parameter]
    if not filters:
        raise Exception("No valid parameters provided")
    requested_cellular_towers = queryset.filter(**filters)
    if requested_cellular_towers is None:
        raise Exception("No cellulartower found with these parameters")
    return requested_cellular_towers
