import pytest
from api.core.cellulartower import CellularTower


@pytest.fixture(scope="module")
def new_cellulartower():
    cellular_tower = CellularTower(
        radio="GSM",
        mcc=270,
        net=10,
        area=12,
        cell=1,
        unit=1,
        lon="1.0000000000",
        lat="1.0000000000",
        range=1,
        samples=1,
        changeable=1,
        created=1,
        updated=1,
        averageSignal=1
    )
    return cellular_tower