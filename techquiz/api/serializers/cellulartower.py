from api.models.cellulartower import CellularTower
from rest_framework import serializers


class CellularTowerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CellularTower
        fields = [
            'radio',
            'mcc',
            'net',
            'area',
            'cell',
            'unit',
            'lon',
            'lat',
            'range',
            'samples',
            'changeable',
            'created',
            'updated',
            'averageSignal'
        ]